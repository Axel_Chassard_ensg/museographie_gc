#!/bin/bash

function launchServer() {
    python3 app_server/manage.py runserver
}

function launchWebInterface() {
    cd web_interface
    quasar dev
}
