import unittest
from datetime import datetime, date

from app_server.gallery.engine.date_manager import DateManager


class TestDateManager(unittest.TestCase):
    def setUp(self) -> None:
        self._date_manager = DateManager()

    def test_correct_date_in_isoformat(self):
        # arrange
        metadata_images = [('portrait_gc_nb', date(1914, 8, 2))]

        # act
        date_corrected = self._date_manager.correct_date_in_isoformat(metadata_images)

        # assert
        self.assertEqual('1914-08-02',date_corrected[0][1])