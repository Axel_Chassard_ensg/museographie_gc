from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('firstimage', views.send_first_image, name='firstimage'),
    path('imagemeta', views.send_image_metadata, name='imagemeta'),
    path('image/<type>/<name>/', views.send_specific_image, name='specificimage'),
]