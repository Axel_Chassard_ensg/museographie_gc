from bson import json_util
from django.http import HttpResponse, FileResponse

from gallery.engine.image_manager import ImageManager
from gallery.engine.postgres_dao import PostgresDAO

FIRST_IMAGE = 'photo_haut.tif'
IMAGE_PATH = 'public'

def index(request):
    return HttpResponse("This is the applicative server of the project of Georges Chassard museography")

def send_first_image(request):
    image_manager = ImageManager()
    first_image = image_manager.get_first_image(image_path=IMAGE_PATH,first_image_name=FIRST_IMAGE)
    return FileResponse(first_image)

def send_image_metadata(request):
    postgres_dao = PostgresDAO()
    metadata_images = postgres_dao.get_metadata_images()
    return HttpResponse(json_util.dumps(metadata_images), content_type="application/json")

def send_specific_image(request,type,name):
    extension = 'jpg'
    if type == 'image_egalise':
        extension = 'png'
    image = open(f'./public/{type}/{name}.{extension}','rb')
    return FileResponse(image)
