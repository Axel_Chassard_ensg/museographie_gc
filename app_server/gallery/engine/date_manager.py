from typing import List


class DateManager:

    def correct_date_in_isoformat(self, metadata_images)->List:
        new_metada_images = []
        for tuple in metadata_images:
            date = tuple[1]
            new_metada_images.append((tuple[0],date.isoformat()))
        return new_metada_images