from gallery.engine.date_manager import DateManager
import psycopg2

import environ
env = environ.Env()
environ.Env.read_env()


class PostgresDAO:
    def __init__(self):
        self._date_manager = DateManager()

    def get_metadata_images(self):
        connection = psycopg2.connect(
            database=env("DB_NAME"), user=env("DB_USER"), password=env("DB_PASSWORD"), host=env("DB_HOST"), port=env("DB_PORT")
        )
        cursor = connection.cursor()
        cursor.execute('''SELECT * from meta_image_gc''')
        result = cursor.fetchall()
        result = self._date_manager.correct_date_in_isoformat(result)
        connection.commit()
        connection.close()
        return result
