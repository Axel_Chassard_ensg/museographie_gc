from typing import BinaryIO

class ImageManager:
    def get_first_image(self, image_path:str, first_image_name:str)-> BinaryIO:
        return open(f'{image_path}/original/{first_image_name}','rb')