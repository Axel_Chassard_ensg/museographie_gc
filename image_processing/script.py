from skimage import io as io
from skimage import exposure
import matplotlib.pyplot as plt
import requests
from os import listdir
import json
import requests


def equalize_folder(folderIn,folderOut):
    for f in listdir(folderIn):
        img = io.imread('{}/{}'.format(folderIn,f))
        img_eq = exposure.equalize_hist(img)
        filename = f.split('.')[0]    #without the extension
        io.imsave('{}/{}.png'.format(folderOut,filename),img_eq)
    return

def colorize_folder(folderIn,folderOut):
    for f in listdir(folderIn):
        r = requests.post(
            "https://api.deepai.org/api/colorizer",
            files={
                'image': open('{}/{}'.format(folderIn,f), 'rb'),
            },
            headers={'api-key': '407c94bb-1383-42f9-bad1-f9923b313a03'}
        )
        rDict = r.json()
        url = rDict['output_url']
        r = requests.get(url, allow_redirects=True)

        open('{}/{}.jpg'.format(folderOut,f.split('.')[0]), 'wb').write(r.content)

        print('colorize : {}'.format(f))
    return

if __name__ == "__main__":

    equalize_folder('original', 'image_egalise')
    colorize_folder('image_egalise','image_egalise_colorie')
    colorize_folder('original','image_colorie')
    
    
    
